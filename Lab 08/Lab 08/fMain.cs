﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_08
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void tbTownsInfo_TextChanged(object sender, EventArgs e)
        {

        }

        private void fMain_Load(object sender, EventArgs e)
        {

        }

        private void btnAddTelephone_Click(object sender, EventArgs e)
        {
            Telephone telephone = new Telephone();
            fTelephone fT = new fTelephone(telephone);
            if (fT.ShowDialog() == DialogResult.OK)
            {
                ;
            }

            {
                tbTelephoneInfo.Text += string.Format("{0},\r\n{1},\r\n №.{2},\r\n {3}GB.,\r\n {4}GB.,\r\n {5}UAH.,\r\n {6},\r\n {7}\r\n\r\n", telephone.NameTelephone, telephone.TelephoneModel, telephone.ProcessorModel, telephone.RAM, telephone.Memory, telephone.Price, telephone.PopularityInUkraine ? "Популярна" : "Не популярна", telephone.CheaperVersion ? "Є бюджетна версія" : "Немає бюджетної версії");

            }
                

        }
    }
}
