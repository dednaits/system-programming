﻿namespace Lab_08
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTelephoneInfo = new System.Windows.Forms.TextBox();
            this.btnAddTelephone = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbTelephoneInfo
            // 
            this.tbTelephoneInfo.Location = new System.Drawing.Point(12, 12);
            this.tbTelephoneInfo.Multiline = true;
            this.tbTelephoneInfo.Name = "tbTelephoneInfo";
            this.tbTelephoneInfo.ReadOnly = true;
            this.tbTelephoneInfo.Size = new System.Drawing.Size(667, 372);
            this.tbTelephoneInfo.TabIndex = 0;
            this.tbTelephoneInfo.TextChanged += new System.EventHandler(this.tbTownsInfo_TextChanged);
            // 
            // btnAddTelephone
            // 
            this.btnAddTelephone.Location = new System.Drawing.Point(685, 10);
            this.btnAddTelephone.Name = "btnAddTelephone";
            this.btnAddTelephone.Size = new System.Drawing.Size(103, 23);
            this.btnAddTelephone.TabIndex = 1;
            this.btnAddTelephone.Text = "Додати телефон";
            this.btnAddTelephone.UseVisualStyleBackColor = true;
            this.btnAddTelephone.Click += new System.EventHandler(this.btnAddTelephone_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(685, 361);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(103, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 396);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAddTelephone);
            this.Controls.Add(this.tbTelephoneInfo);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Залікове завдання";
            this.Load += new System.EventHandler(this.fMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTelephoneInfo;
        private System.Windows.Forms.Button btnAddTelephone;
        private System.Windows.Forms.Button btnClose;
    }
}

