﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_08
{
    public partial class fTelephone : Form
     {
        public Telephone TheTelephone;

        public object Button1 { get; private set; }

        public fTelephone( Telephone T)
        {
            TheTelephone = T;
            InitializeComponent();
        }

        private void fTelephone_Load(object sender, EventArgs e)
        {
            if (TheTelephone != null)
            {
                tbName.Text = TheTelephone.NameTelephone;
                tbModel.Text = TheTelephone.TelephoneModel;
                tbProcessorModel.Text = TheTelephone.ProcessorModel;
                tbRAM.Text = TheTelephone.RAM.ToString();
                tbMemory.Text = TheTelephone.Memory.ToString();
                tbPrice.Text = TheTelephone.Price.ToString();
                chbPopularityInUkraine.Checked = TheTelephone.PopularityInUkraine; chbCheaperVersion.Checked = TheTelephone.CheaperVersion;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            TheTelephone.NameTelephone = tbName.Text.Trim();
            TheTelephone.TelephoneModel = tbModel.Text.Trim();
            TheTelephone.ProcessorModel = tbProcessorModel.Text.Trim();
            TheTelephone.RAM = int.Parse(tbRAM.Text.Trim());
            TheTelephone.Memory = int.Parse(tbMemory.Text.Trim());
            TheTelephone.Price = int.Parse(tbPrice.Text.Trim());
            TheTelephone.PopularityInUkraine = chbPopularityInUkraine.Checked;
            TheTelephone.CheaperVersion = chbCheaperVersion.Checked;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void fTelephone_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyValue == (char)Keys.Enter)
            {
                btnCancel_Click(btnCancel, null);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            fForm f = new fForm();
            f.ShowDialog();
        }
    }
}
