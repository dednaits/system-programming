﻿
namespace Lab_08
{
    partial class fForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fMain = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.fMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // fMain
            // 
            this.fMain.Controls.Add(this.textBox1);
            this.fMain.Location = new System.Drawing.Point(12, 12);
            this.fMain.Name = "fMain";
            this.fMain.Size = new System.Drawing.Size(162, 119);
            this.fMain.TabIndex = 0;
            this.fMain.TabStop = false;
            this.fMain.Text = "Данні поля";
            this.fMain.Enter += new System.EventHandler(this.fMain_Enter);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(29, 50);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(150, 146);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // fForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 181);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.fMain);
            this.MaximizeBox = false;
            this.Name = "fForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Window";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.fMain.ResumeLayout(false);
            this.fMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox fMain;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnClose;
    }
}