﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_08
{
    public class Telephone
    {
            public string NameTelephone;
            public string TelephoneModel;
            public string ProcessorModel;
            public int RAM;
            public int Memory;
            public int Price;
            public bool PopularityInUkraine;
            public bool CheaperVersion;

        public TextBox Name { get; internal set; }
        public TextBox Model { get; internal set; }
        public TextBox Processor { get; internal set; }
    }
}
