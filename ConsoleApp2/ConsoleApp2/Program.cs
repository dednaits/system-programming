﻿using System;

namespace lab02
{
    class Program
    {

        private static double Func(double x1, double x2)
        {
            double y;
            return y = Math.Pow(Math.Cos(x1 - Math.Sqrt(x2 / (x1 + 53 * (x2 * x2)))), 4);
        }

        private static void GetUserInput(out double x2Max, out double dx1, out double dx2, out double x1Min, out double x1Max, out double x2Min)
        {
            Console.Write("Enter x1 Min:");
            x1Max = double.Parse(Console.ReadLine());

            Console.Write("Enter x1 Max:");
            x1Min = double.Parse(Console.ReadLine());



            Console.Write("Enter x2 Min:");
            x2Min = double.Parse(Console.ReadLine());
            Console.Write("Enter x2 Max:");
            x2Max = double.Parse(Console.ReadLine());

            Console.Write("Enter dx1:");
            dx1 = double.Parse(Console.ReadLine());

            Console.Write("Enter dx2:");
            dx2 = double.Parse(Console.ReadLine());


        }
        private static void Tabulate(double x2Max, double x2Min, double x1Min, double x1Max, double dx1, double dx2)
        {
            Console.WriteLine("*******{0}*******", "Tabulation");
            double x1 = x1Min;
            double x2 = x2Min;

            double sum = 0;

            for (double i = x1; i <= x1Max; i += dx1)
            {
                for (double j = x2; j <= x2Max; j += dx2)
                {


                    double y = Func(i, j);

                    Console.WriteLine("x1={0}\t\tx2={1} \t\ty={2}", i, j, Math.Round(y, 3));

                }

            }
            Console.WriteLine("*******{0}*******", "Sum");

            Console.WriteLine("Sum={0}", Math.Round(sum, 3));

            Console.WriteLine("*******{0}*******", "Fin");


        }
        static void Main(string[] args)
        {
            GetUserInput(out double x2Max, out double dx1, out double dx2, out double x1Min, out double x1Max, out double x2Min);
            Tabulate(x2Max, x2Min, x1Max, x1Min, dx1, dx2);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}