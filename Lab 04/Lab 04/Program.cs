﻿using System;

namespace Lab04
{
    class Program
    {
        static double Func2(double x1, double x2)
        {
            double a;
            return Math.Log10(x1 * (x2 * x2)) + 45 * Math.Sin(x1 + x2);//  A^1/5 = корень с индексом 5
        }



        static void Main(string[] args)
        {
            double startX = 1, dx = 0.5;
            const int size = 10;
            double[] arr = ClassLibraryLab04.Class1.CreateArrayFromTemplate(startX, dx, size, x => Func2(x * 2.76, x * 0.5));
            ClassLibraryLab04.Class1.BubbleSort(arr);
            ClassLibraryLab04.Class1.PrintArray(arr);

            ClassLibraryLab04.Class1.ProcessArray(arr, out double min, out double max, out double avg);

            Console.WriteLine("Min={0:#0.####}, Max={1:#0.####}, Avg={2:#0.####}", min, max, avg);

            int count = ClassLibraryLab04.Class1.PostProcessArray(arr, x => x >= 0.9 * avg && x <= 1.1 * avg);
            Console.WriteLine("R={0}", count);
            Console.WriteLine();
        }
    }


    namespace ClassLibraryLab04
    {
        public class Class1
        {
            public static double[] CreateArrayFromTemplate(double startX, double dx, int size, Func<double, double> func)
            {
                double[] data = new double[size];
                double x = startX;
                for (int i = 0; i < size; ++i)
                {
                    data[i] = func(x);
                    x += dx;
                }
                return data;
            }

            public static void BubbleSort(double[] data)
            {
                for (int i = 0; i < data.Length; i++)
                {
                    for (int j = i + 1; j < data.Length; j++)
                    {
                        if (data[i] < data[j])
                        {
                            double t = data[i];
                            data[i] = data[j];
                            data[j] = t;
                        }
                    }

                }
            }
            public static void PrintArray(double[] data)
            {
                for (int i = data.GetLowerBound(0); i <= data.GetUpperBound(0); ++i)
                {
                    Console.WriteLine("arr[{0}]={1:#0.####}", i, data[i]);
                }
            }

            public static void ProcessArray(double[] data, out double aMin, out double aMax, out double aAvg)
            {
                double S = data[0];
                aMin = aMax = S;
                for (int i = data.GetLowerBound(0) + 1; i <= data.GetUpperBound(0); ++i)
                {
                    S += data[i];

                    if (aMin > data[i])
                    {
                        aMin = data[i];
                    }

                    if (aMax < data[i])
                    {
                        aMax = data[i];
                    }
                }

                aAvg = S / data.Length;
            }

            public static int PostProcessArray(double[] data, Predicate<double> pred)
            {
                int count = 0;
                for (int i = data.GetLowerBound(0); i <= data.GetUpperBound(0); ++i)
                {
                    if (pred(data[i]))
                    {
                        ++count;
                    }
                }

                return count;
            }
        }
    }
}


