﻿using System;

namespace ClassLibrary_lab05
{
    public class Class1
    {
        public string NameTelephone;
        public string TelephoneModel;
        public string ProcessorModel;
        public int RAM;
        public int Memory;
        public int Price;
        public bool PopularityInUkraine;
        public bool CheaperVersion;
        public static void Telephone()
        {
            Console.WriteLine("Введiть марку телефону: ");
            string sNameTelephone = Console.ReadLine();
            Console.WriteLine("Введiть модель телефону: ");
            string sTelephoneModel = Console.ReadLine();
            Console.WriteLine("Введiть модель процессору: ");
            string sProcessorModel = Console.ReadLine();
            Console.WriteLine("Введiть кількість оперативної пам'яті: ");
            string sRAM = Console.ReadLine();
            Console.WriteLine("Введiть кількість вбудованної пам'яті: ");
            string sMemory = Console.ReadLine();
            Console.WriteLine("Введiть ціну телефону в грн: ");
            string sPrice = Console.ReadLine();

            Console.Write("Чи популярна ця модель телефону в нашій країні? (t-так, n-нi): ");

            ConsoleKeyInfo keyPopularityInUkraine = Console.ReadKey();
            Console.Write("Чи є бюджетна версія цього телефону? (t-так, n-нi): ");
            ConsoleKeyInfo keyCheaperVersion = Console.ReadKey();


            Class1 OurTelephone = new Class1
            {
                NameTelephone = sNameTelephone,
                TelephoneModel = sTelephoneModel,
                ProcessorModel = sProcessorModel,
                RAM = int.Parse(sRAM),
                Memory = int.Parse(sMemory),
                Price = int.Parse(sPrice),
                PopularityInUkraine = keyPopularityInUkraine.Key == ConsoleKey.T ? true : false,
                CheaperVersion = keyCheaperVersion.Key == ConsoleKey.T ? true : false
            };

            Console.WriteLine();
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Данi про об`ект: ");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Марка телефону: " + OurTelephone.NameTelephone);
            Console.WriteLine("Його модель: " + OurTelephone.TelephoneModel);
            Console.WriteLine("Модель процессору: " + OurTelephone.ProcessorModel);
            Console.WriteLine("Кількість оперативної пам'яті: " +
             OurTelephone.RAM.ToString());
            Console.WriteLine("Кількість вбудованої пам'яті: " +
             OurTelephone.Memory.ToString());
            Console.WriteLine("Ціна телефону: " +
             OurTelephone.Price.ToString("грн."));
            Console.WriteLine("Популярність моделі в нашій країні: " +
             OurTelephone.PopularityInUkraine.ToString());
            Console.WriteLine(OurTelephone.CheaperVersion ? "Чи є бюджетна версія" :
             "Немає бюджетної версії");
            Console.ReadKey();
        }
    }
}
